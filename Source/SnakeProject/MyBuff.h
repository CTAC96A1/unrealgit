// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Food.h"
#include "MyBuff.generated.h"

/**
 * 
 */
UCLASS()
class SNAKEPROJECT_API AMyBuff : public AFood
{
	GENERATED_BODY()


public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void Interact(AActor* Interactor, bool bIsHead) override;
	
	virtual void Spawn();
};
