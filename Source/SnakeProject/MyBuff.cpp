// Fill out your copyright notice in the Description page of Project Settings.


#include "MyBuff.h"
#include "SnakeBase.h"

void AMyBuff::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AMyBuff::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->AddSnakeElement(2);
			/*Snake->Timer(15);*/
			Spawn();
			this->Destroy();

			/*Snake->MovementSpeed = 25.f;
			Snake->Timer(15);
			AFood().Spawn();
			this->Destroy();*/
		}
	}
}

void AMyBuff::Spawn()
{
	FVector Location(FMath::RandRange(-460, 460), FMath::RandRange(-460, 460), 0);
	FActorSpawnParameters SpawnParams;
	FRotator rotator;

	int32 randNum = FMath().FRandRange(1, 100);
	UE_LOG(LogTemp, Warning, TEXT("%d"), randNum);
	if (randNum >= 0 && randNum <= 20)//[0,20] - 20% Вероятность
	{
		if (ToSpawnBuff)
		{
			AFood* food = GetWorld()->SpawnActor<AFood>(ToSpawnBuff, Location, rotator, SpawnParams);
		}
	}
	else
	{
		if (randNum >= 21 && randNum <= 40)//[21,40] - 20% Вероятность
		{
			if (ToSpawnDebuff)
			{
				AFood* food = GetWorld()->SpawnActor<AFood>(ToSpawnDebuff, Location, rotator, SpawnParams);
			}
		}
		else//[40,100] - 60% Вероятность
		{
			if (ToSpawn)
			{
				AFood* food = GetWorld()->SpawnActor<AFood>(ToSpawn, Location, rotator, SpawnParams);
			}
		}
	}


}
