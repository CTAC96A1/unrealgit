// Fill out your copyright notice in the Description page of Project Settings.


#include "Food.h"
#include "SnakeBase.h"

// Sets default values
AFood::AFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void AFood::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AFood::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->AddSnakeElement(1);
			Spawn();
			this->Destroy();
			
		}
	}
}



void AFood::Spawn()
{
	FVector Location(FMath::RandRange(-460, 460), FMath::RandRange(-460, 460), 0);
	FActorSpawnParameters SpawnParams;
	FRotator rotator;

	int32 randNum = FMath().FRandRange(1, 100);
	UE_LOG(LogTemp, Warning, TEXT("%d"), randNum);
	if (randNum >= 0 && randNum <= 20)//[0,20] - 20% Вероятность
	{
		if (ToSpawnBuff)
		{
			AFood* food = GetWorld()->SpawnActor<AFood>(ToSpawnBuff, Location, rotator, SpawnParams);
		}
	}
	else
	{
		if (randNum >= 21 && randNum <= 40)//[21,40] - 20% Вероятность
		{
			if (ToSpawnDebuff)
			{
				AFood* food = GetWorld()->SpawnActor<AFood>(ToSpawnDebuff, Location, rotator, SpawnParams);
			}
		}
		else//[40,100] - 60% Вероятность
		{
			if (ToSpawn)
			{
				AFood* food = GetWorld()->SpawnActor<AFood>(ToSpawn, Location, rotator, SpawnParams);
			}
		}
	}

}


