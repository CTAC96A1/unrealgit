// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SNAKEPROJECT_SnakeProjectGameModeBase_generated_h
#error "SnakeProjectGameModeBase.generated.h already included, missing '#pragma once' in SnakeProjectGameModeBase.h"
#endif
#define SNAKEPROJECT_SnakeProjectGameModeBase_generated_h

#define ProjectGit_Source_SnakeProject_SnakeProjectGameModeBase_h_15_SPARSE_DATA
#define ProjectGit_Source_SnakeProject_SnakeProjectGameModeBase_h_15_RPC_WRAPPERS
#define ProjectGit_Source_SnakeProject_SnakeProjectGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define ProjectGit_Source_SnakeProject_SnakeProjectGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASnakeProjectGameModeBase(); \
	friend struct Z_Construct_UClass_ASnakeProjectGameModeBase_Statics; \
public: \
	DECLARE_CLASS(ASnakeProjectGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/SnakeProject"), NO_API) \
	DECLARE_SERIALIZER(ASnakeProjectGameModeBase)


#define ProjectGit_Source_SnakeProject_SnakeProjectGameModeBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesASnakeProjectGameModeBase(); \
	friend struct Z_Construct_UClass_ASnakeProjectGameModeBase_Statics; \
public: \
	DECLARE_CLASS(ASnakeProjectGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/SnakeProject"), NO_API) \
	DECLARE_SERIALIZER(ASnakeProjectGameModeBase)


#define ProjectGit_Source_SnakeProject_SnakeProjectGameModeBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASnakeProjectGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASnakeProjectGameModeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASnakeProjectGameModeBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASnakeProjectGameModeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASnakeProjectGameModeBase(ASnakeProjectGameModeBase&&); \
	NO_API ASnakeProjectGameModeBase(const ASnakeProjectGameModeBase&); \
public:


#define ProjectGit_Source_SnakeProject_SnakeProjectGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASnakeProjectGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASnakeProjectGameModeBase(ASnakeProjectGameModeBase&&); \
	NO_API ASnakeProjectGameModeBase(const ASnakeProjectGameModeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASnakeProjectGameModeBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASnakeProjectGameModeBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASnakeProjectGameModeBase)


#define ProjectGit_Source_SnakeProject_SnakeProjectGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET
#define ProjectGit_Source_SnakeProject_SnakeProjectGameModeBase_h_12_PROLOG
#define ProjectGit_Source_SnakeProject_SnakeProjectGameModeBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ProjectGit_Source_SnakeProject_SnakeProjectGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	ProjectGit_Source_SnakeProject_SnakeProjectGameModeBase_h_15_SPARSE_DATA \
	ProjectGit_Source_SnakeProject_SnakeProjectGameModeBase_h_15_RPC_WRAPPERS \
	ProjectGit_Source_SnakeProject_SnakeProjectGameModeBase_h_15_INCLASS \
	ProjectGit_Source_SnakeProject_SnakeProjectGameModeBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define ProjectGit_Source_SnakeProject_SnakeProjectGameModeBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ProjectGit_Source_SnakeProject_SnakeProjectGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	ProjectGit_Source_SnakeProject_SnakeProjectGameModeBase_h_15_SPARSE_DATA \
	ProjectGit_Source_SnakeProject_SnakeProjectGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	ProjectGit_Source_SnakeProject_SnakeProjectGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
	ProjectGit_Source_SnakeProject_SnakeProjectGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SNAKEPROJECT_API UClass* StaticClass<class ASnakeProjectGameModeBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID ProjectGit_Source_SnakeProject_SnakeProjectGameModeBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
