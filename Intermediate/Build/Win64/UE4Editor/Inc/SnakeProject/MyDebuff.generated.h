// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SNAKEPROJECT_MyDebuff_generated_h
#error "MyDebuff.generated.h already included, missing '#pragma once' in MyDebuff.h"
#endif
#define SNAKEPROJECT_MyDebuff_generated_h

#define ProjectGit_Source_SnakeProject_MyDebuff_h_15_SPARSE_DATA
#define ProjectGit_Source_SnakeProject_MyDebuff_h_15_RPC_WRAPPERS
#define ProjectGit_Source_SnakeProject_MyDebuff_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define ProjectGit_Source_SnakeProject_MyDebuff_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAMyDebuff(); \
	friend struct Z_Construct_UClass_AMyDebuff_Statics; \
public: \
	DECLARE_CLASS(AMyDebuff, AFood, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/SnakeProject"), NO_API) \
	DECLARE_SERIALIZER(AMyDebuff)


#define ProjectGit_Source_SnakeProject_MyDebuff_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAMyDebuff(); \
	friend struct Z_Construct_UClass_AMyDebuff_Statics; \
public: \
	DECLARE_CLASS(AMyDebuff, AFood, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/SnakeProject"), NO_API) \
	DECLARE_SERIALIZER(AMyDebuff)


#define ProjectGit_Source_SnakeProject_MyDebuff_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMyDebuff(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMyDebuff) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMyDebuff); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMyDebuff); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMyDebuff(AMyDebuff&&); \
	NO_API AMyDebuff(const AMyDebuff&); \
public:


#define ProjectGit_Source_SnakeProject_MyDebuff_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMyDebuff() { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMyDebuff(AMyDebuff&&); \
	NO_API AMyDebuff(const AMyDebuff&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMyDebuff); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMyDebuff); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AMyDebuff)


#define ProjectGit_Source_SnakeProject_MyDebuff_h_15_PRIVATE_PROPERTY_OFFSET
#define ProjectGit_Source_SnakeProject_MyDebuff_h_12_PROLOG
#define ProjectGit_Source_SnakeProject_MyDebuff_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ProjectGit_Source_SnakeProject_MyDebuff_h_15_PRIVATE_PROPERTY_OFFSET \
	ProjectGit_Source_SnakeProject_MyDebuff_h_15_SPARSE_DATA \
	ProjectGit_Source_SnakeProject_MyDebuff_h_15_RPC_WRAPPERS \
	ProjectGit_Source_SnakeProject_MyDebuff_h_15_INCLASS \
	ProjectGit_Source_SnakeProject_MyDebuff_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define ProjectGit_Source_SnakeProject_MyDebuff_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ProjectGit_Source_SnakeProject_MyDebuff_h_15_PRIVATE_PROPERTY_OFFSET \
	ProjectGit_Source_SnakeProject_MyDebuff_h_15_SPARSE_DATA \
	ProjectGit_Source_SnakeProject_MyDebuff_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	ProjectGit_Source_SnakeProject_MyDebuff_h_15_INCLASS_NO_PURE_DECLS \
	ProjectGit_Source_SnakeProject_MyDebuff_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SNAKEPROJECT_API UClass* StaticClass<class AMyDebuff>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID ProjectGit_Source_SnakeProject_MyDebuff_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
