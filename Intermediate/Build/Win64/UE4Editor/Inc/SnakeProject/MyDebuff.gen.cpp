// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "SnakeProject/MyDebuff.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMyDebuff() {}
// Cross Module References
	SNAKEPROJECT_API UClass* Z_Construct_UClass_AMyDebuff_NoRegister();
	SNAKEPROJECT_API UClass* Z_Construct_UClass_AMyDebuff();
	SNAKEPROJECT_API UClass* Z_Construct_UClass_AFood();
	UPackage* Z_Construct_UPackage__Script_SnakeProject();
// End Cross Module References
	void AMyDebuff::StaticRegisterNativesAMyDebuff()
	{
	}
	UClass* Z_Construct_UClass_AMyDebuff_NoRegister()
	{
		return AMyDebuff::StaticClass();
	}
	struct Z_Construct_UClass_AMyDebuff_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AMyDebuff_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AFood,
		(UObject* (*)())Z_Construct_UPackage__Script_SnakeProject,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMyDebuff_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "IncludePath", "MyDebuff.h" },
		{ "ModuleRelativePath", "MyDebuff.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_AMyDebuff_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AMyDebuff>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AMyDebuff_Statics::ClassParams = {
		&AMyDebuff::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_AMyDebuff_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AMyDebuff_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AMyDebuff()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AMyDebuff_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AMyDebuff, 210557034);
	template<> SNAKEPROJECT_API UClass* StaticClass<AMyDebuff>()
	{
		return AMyDebuff::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AMyDebuff(Z_Construct_UClass_AMyDebuff, &AMyDebuff::StaticClass, TEXT("/Script/SnakeProject"), TEXT("AMyDebuff"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AMyDebuff);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
