// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "SnakeProject/MyBuff.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMyBuff() {}
// Cross Module References
	SNAKEPROJECT_API UClass* Z_Construct_UClass_AMyBuff_NoRegister();
	SNAKEPROJECT_API UClass* Z_Construct_UClass_AMyBuff();
	SNAKEPROJECT_API UClass* Z_Construct_UClass_AFood();
	UPackage* Z_Construct_UPackage__Script_SnakeProject();
// End Cross Module References
	void AMyBuff::StaticRegisterNativesAMyBuff()
	{
	}
	UClass* Z_Construct_UClass_AMyBuff_NoRegister()
	{
		return AMyBuff::StaticClass();
	}
	struct Z_Construct_UClass_AMyBuff_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AMyBuff_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AFood,
		(UObject* (*)())Z_Construct_UPackage__Script_SnakeProject,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMyBuff_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "IncludePath", "MyBuff.h" },
		{ "ModuleRelativePath", "MyBuff.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_AMyBuff_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AMyBuff>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AMyBuff_Statics::ClassParams = {
		&AMyBuff::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_AMyBuff_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AMyBuff_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AMyBuff()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AMyBuff_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AMyBuff, 597644581);
	template<> SNAKEPROJECT_API UClass* StaticClass<AMyBuff>()
	{
		return AMyBuff::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AMyBuff(Z_Construct_UClass_AMyBuff, &AMyBuff::StaticClass, TEXT("/Script/SnakeProject"), TEXT("AMyBuff"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AMyBuff);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
